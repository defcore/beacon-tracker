'use strict';

var utils = require('../utils/writer.js');
var Developers = require('../service/DevelopersService');

module.exports.beaconEnter = function beaconEnter (req, res, next) {
  var uuid = req.swagger.params['uuid'].value;
  var timestamp = req.swagger.params['timestamp'].value;
  Developers.beaconEnter(uuid,timestamp)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.beaconLeave = function beaconLeave (req, res, next) {
  var uuid = req.swagger.params['uuid'].value;
  var timestamp = req.swagger.params['timestamp'].value;
  Developers.beaconLeave(uuid,timestamp)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
