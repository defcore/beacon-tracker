'use strict';


function extracted(timestamp) {
    var date = new Date(timestamp * 1000);
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime;
}

/**
 * searches inventory
 *
 * uuid String uuid of the beacon
 * timestamp String timestamp
 * no response value expected for this operation
 **/
exports.beaconEnter = function (uuid, timestamp) {
    return new Promise(function (resolve, reject) {

        const formattedTime = extracted(timestamp);
        console.log("beaconEnter " + uuid + " " + formattedTime);

        resolve();
    });
}


/**
 * searches inventory
 *
 * uuid String uuid of the beacon
 * timestamp String timestamp
 * no response value expected for this operation
 **/
exports.beaconLeave = function (uuid, timestamp) {
    return new Promise(function (resolve, reject) {

        const formattedTime = extracted(timestamp);
        console.log("beaconLeave " + uuid + " " + formattedTime);

        resolve();
    });
}

