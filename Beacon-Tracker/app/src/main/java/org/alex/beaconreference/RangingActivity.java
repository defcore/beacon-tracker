package org.alex.beaconreference;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.EditText;

import com.bosphere.filelogger.FL;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.Collection;

public class RangingActivity extends Activity implements BeaconConsumer {
    protected static final String TAG = "RangingActivity";
    private BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranging);
    }

    @Override 
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override 
    protected void onPause() {
        super.onPause();
        beaconManager.unbind(this);
    }

    @Override 
    protected void onResume() {
        super.onResume();
        beaconManager.bind(this);
    }

    @Override
    public void onBeaconServiceConnect() {
        final Context context = this.getApplicationContext();
        RangeNotifier rangeNotifier = new RangeNotifier() {
           @Override
           public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
              if (beacons.size() > 0) {

                  Log.d(TAG, "didRangeBeaconsInRegion called with beacon count:  " + beacons.size());
                  Beacon firstBeacon = beacons.iterator().next();
                  if (firstBeacon.getDistance() < 2.0) {

//                      Log.i("INFO:",  firstBeacon.toString());

//                      Log.i("INFO:",  firstBeacon.getBluetoothAddress());
//                      Log.i("INFO:",  firstBeacon.getParserIdentifier());
//                      Log.i("INFO:",  firstBeacon.getDataFields().toString());
//                      Log.i("INFO:",  firstBeacon.getExtraDataFields().toString());


                      logToDisplay("The first beacon " + firstBeacon.toString() + " is about " + firstBeacon.getDistance() + " meters away.");
                      FL.i("didRangeBeaconsInRegion BluetoothAddress " + firstBeacon.getBluetoothAddress() + "  distance "  + firstBeacon.getDistance() + " meters away");
                      Client.enter(context, "didRangeBeaconsInRegion-" + firstBeacon.getBluetoothAddress());
                  }
              }
           }

        };
        try {
            beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
            beaconManager.startRangingBeaconsInRegion(new Region("4152554e-f99b-4a3b-86d0-947070693a78", null, null, null));
            beaconManager.addRangeNotifier(rangeNotifier);
//            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
//            beaconManager.addRangeNotifier(rangeNotifier);
        } catch (RemoteException e) {   }
    }

    private void logToDisplay(final String line) {
        runOnUiThread(new Runnable() {
            public void run() {
                EditText editText = (EditText)RangingActivity.this.findViewById(R.id.rangingText);
                editText.append(line+"\n");
            }
        });
    }
}
