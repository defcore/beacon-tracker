package org.alex.beaconreference;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by Alex Untertrifaller on 13.03.20.
 */
public class Client {

    public static String TAG = "Client";


    public static void enter(Context context, String tag) {
        // Instantiate the RequestQueue.
//        Context context = BeaconReferenceApplication.getAppContext();
        RequestQueue queue = Volley.newRequestQueue(context);
        long unixTime = System.currentTimeMillis() / 1000L;

        String url = "http://192.168.1.54:8080/defcore/Beacon/1.0.0/beacon/enter/"+tag+"/" + unixTime;

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.i(TAG, "Request done");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "That didn't work!");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public static void leave(Context context) {
        // Instantiate the RequestQueue.
//        Context context = BeaconReferenceApplication.getAppContext();
        RequestQueue queue = Volley.newRequestQueue(context);
        long unixTime = System.currentTimeMillis() / 1000L;

        String url = "http://192.168.1.54:8080/defcore/Beacon/1.0.0/beacon/leave/uuid/" + unixTime;

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.i(TAG, "Request done");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "That didn't work!");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
